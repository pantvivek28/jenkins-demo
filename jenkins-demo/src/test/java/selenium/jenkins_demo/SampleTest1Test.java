package selenium.jenkins_demo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SampleTest1Test {
	private WebDriver driver;	
	
	@AfterTest
  public void afterTest() {
    driver.quit();
  }

	@BeforeTest
  public void beforeTest() {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
  }

  @Test
  public void test1() {
		driver.get("http://demo.guru99.com/test/guru99home/");  
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Demo Guru99 Page")); 
  }
  
  @Test
  public void test2() {
		driver.get("http://demo.guru99.com/test/guru99home/");  
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Demo Guru99 Page")); 
  }
  
  @Test
  public void test3() {
		driver.get("http://demo.guru99.com/test/guru99home/");  
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Demo Guru99 Page")); 
  }
}
